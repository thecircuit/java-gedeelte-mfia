package sample;
import com.google.gson.*;
import java.io.FileReader;
import java.io.Reader;

public class json {
        public static void main(String[] args) throws Exception {
            try (Reader reader = new FileReader("results.json")) {
                JsonParser parser = new JsonParser();
                JsonObject f = (JsonObject)parser.parse(reader);
                if (f.has("measurement0")){
                    JsonObject x = f.get("measurement0").getAsJsonObject();
                    JsonObject x2 = x.get("sweep_result_0").getAsJsonObject();
                    JsonArray x3 = x2.get("phase").getAsJsonArray();
                    System.out.print(x3);

                }
            }
        }

}
