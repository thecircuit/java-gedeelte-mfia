package sample;

import java.io.*;

public class ProcessTest
    {
        public static void main(String[] args)
        {
            //this will start a python program in java
            try {
                ProcessBuilder pb = new ProcessBuilder().command("python",
                        "E:\\Dropbox\\1school\\jaar3\\java2\\pythonTest\\src\\sample\\tesFilepythonjava.py",
                        "test",
                        "test3");
                Process p = pb.start();
                BufferedReader ins = new BufferedReader(new InputStreamReader(p.getInputStream()));
                BufferedReader insError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                String line;
                while (p.isAlive()){
                    while((line=ins.readLine())!=null){
                        System.out.println(line);

                    }
                    while((line=insError.readLine())!=null){
                        System.out.println(line);
                    }
                }

                System.out.println("this is the exit value " + p.exitValue());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }