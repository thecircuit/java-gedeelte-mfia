
import datetime
import sys

def create_file(name, header_string):
    with open(name + '.txt', 'w', encoding="utf8") as f:
        f.write("\ntest started at " + str(datetime.datetime.now()))
        f.write("\n"+header_string)


def open_file(name):
    with open(name + '.txt', 'r', encoding="utf8") as f:
        s = f.read()
        print(s)


print("line 1: " + str(len(sys.argv)))
print("line 2: " + str(sys.argv))
create_file("testy",sys.argv[2])
open_file("testy")