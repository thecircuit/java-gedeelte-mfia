package GUI_MFIA.Sweeper;

import javafx.fxml.FXML;
import javafx.scene.control.*;

public class ControllerSweeper {

    @FXML
    public Button bExecute,bRemove,bAdd,bMenu;
    @FXML
    TreeView<String> tvItems;
    @FXML
    ChoiceBox<String> cbRemove;
    @FXML
    TextArea taConsole;
    @FXML
    public MenuItem miMenu;

}
