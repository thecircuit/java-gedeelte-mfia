package GUI_MFIA.DataViewer;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ControllerDataViewer implements Initializable {


    @FXML
    public MenuItem miMenu;
    @FXML
    Button bSelectDataFile;
    @FXML
    StackPane itemTreeView;

    TreeItem<String> item;

    @FXML
    TextArea taConsole;

    private final Node rootIcon = new ImageView(
            new Image(getClass().getResourceAsStream("resources/folder-open-add.png"))
    );

    private FileChooser chooser = new FileChooser();

    @Override
    public void initialize(URL location, ResourceBundle resources){
        //setting up textarea
        taConsole.setEditable(false);

        //setting up the file chooser window
        chooser.setTitle("Select json mfia results file");
        chooser.setInitialDirectory(new File(System.getProperty("user.home")));
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("TXT", "*.txt"));
    }
    TreeView<String> tree;
    @FXML
    public void mcSelectFile() {
        FileChooser chooser = new FileChooser();
        String file = chooser.showOpenDialog(bSelectDataFile.getScene().getWindow()).getPath();
        ArrayList<String> al = getJsonMeasurements(file);
        TreeItem<String> rootItem = new TreeItem<String>("testing", rootIcon);
        al.forEach(
                n -> {
                    TreeItem<String> it = new TreeItem<>("lol");
                    rootItem.getChildren().add(new TreeItem<>( n, new ImageView(
                            new Image(getClass().getResourceAsStream("resources/2993862.png")))));
                }
        );
        tree = new TreeView<String>(rootItem);
        taConsole.appendText("readed file");
        itemTreeView.getChildren().add(tree);
        tree.setOnMouseClicked(event -> taConsole.appendText("\n" + tree.getSelectionModel().getSelectedItem().getValue()));
    }
    @FXML
    public void mcAdd(){
    }

    private boolean isJsonResult(String url){
        return true;
    }

    private ArrayList<String> getJsonMeasurements(String path){

        ArrayList<String> al = new ArrayList<>();

        try (Reader reader = new FileReader(path)) {
            JsonParser parser = new JsonParser();
            JsonObject f = (JsonObject)parser.parse(reader);
            boolean flag = true;
            int i=0;
            while (flag){
            if (f.has("measurement" + i)){
               al.add("measurement" + i);
            }else{
                flag = false;
            }
            i++;
            }
        }catch (Exception e){e.printStackTrace();}
        return al;
    }

}
