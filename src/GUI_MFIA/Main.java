package GUI_MFIA;

import GUI_MFIA.DataViewer.ControllerDataViewer;
import GUI_MFIA.Info.ControllerInfo;
import GUI_MFIA.Sweeper.ControllerSweeper;
import GUI_MFIA.settingsMaker.ControllerSettingsMaker;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {
    @FXML
    private FXMLLoader load;
    @FXML
    private Parent choiceParent;
    @FXML
    private Scene choiceScene,infoScene,sweeperScene,dataViewScene,testMakeScene;
    @FXML
    private Stage appWindow,appPrimWindow;

    private boolean notCloseFlag = true;

    @Override
    public void start(Stage primaryStage) throws Exception{
        //preload all scenes
        sceneMenu();
        sceneDataViewer();
        sceneInfo();
        sceneSweeper();
        sceneDataSetMaker();
        appPrimWindow = primaryStage;

        while(notCloseFlag) {
            appWindow.setScene(choiceScene);
            appWindow.showAndWait();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
    private void sceneMenu() throws Exception{
        //choice screen controller
        appWindow = new Stage();
        appWindow.initModality(Modality.APPLICATION_MODAL);
        load = new FXMLLoader(getClass().getResource("mainMenu.fxml"));
        choiceParent = load.load();
        choiceScene = new Scene(choiceParent);
        //choiceScene.getStylesheets().add(getClass().getResource("userInterface/Styleprogam2.css").toString());
        ControllerMenu lc = load.getController();
        appWindow.setTitle("Menu MFIA APP");
        lc.bCompData.setOnMouseClicked(event -> appWindow.setScene(dataViewScene));
        lc.bExit.setOnMouseClicked(event -> {
            notCloseFlag = false;
            appWindow.close();
        });
        lc.bInfo.setOnMouseClicked(event -> appWindow.setScene(infoScene));
        lc.bMakeSet.setOnMouseClicked(event -> appWindow.setScene(testMakeScene));
        lc.bRunTest.setOnMouseClicked(event -> appWindow.setScene(sweeperScene));
    }

    private void sceneDataViewer() throws Exception{
        //spelling controller
        FXMLLoader load2 = new FXMLLoader(getClass().getResource("DataViewer/dataViewer.fxml"));
        Parent assigment1Parent = load2.load();
        dataViewScene = new Scene(assigment1Parent);
        //sweeperScene.getStylesheets().add(getClass().getResource("userInterface/Styleprogam2.css").toString());
        ControllerDataViewer cSC = load2.getController();
        cSC.miMenu.setOnAction(event -> appWindow.setScene(choiceScene));
    }

    private void sceneDataSetMaker() throws Exception{
        //spelling controller
        FXMLLoader load2 = new FXMLLoader(getClass().getResource("settingsMaker/settingsMaker.fxml"));
        Parent assigment1Parent = load2.load();
        testMakeScene = new Scene(assigment1Parent);
        //sweeperScene.getStylesheets().add(getClass().getResource("userInterface/Styleprogam2.css").toString());
        ControllerSettingsMaker cSC = load2.getController();
        cSC.miMenu.setOnAction(event -> appWindow.setScene(choiceScene));
    }
    private void sceneSweeper() throws Exception{
        //spelling controller
        FXMLLoader load2 = new FXMLLoader(getClass().getResource("Sweeper/sweeper.fxml"));
        Parent assigment1Parent = load2.load();
        sweeperScene = new Scene(assigment1Parent);
        //sweeperScene.getStylesheets().add(getClass().getResource("userInterface/Styleprogam2.css").toString());
        ControllerSweeper cSC = load2.getController();
        cSC.bMenu.setOnMouseClicked(event -> appWindow.setScene(choiceScene));
        cSC.miMenu.setOnAction(event -> appWindow.setScene(choiceScene));
    }
    private void sceneInfo() throws Exception{
        //spelling controller
        FXMLLoader load2 = new FXMLLoader(getClass().getResource("Info/info.fxml"));
        Parent assigment1Parent = load2.load();
        infoScene = new Scene(assigment1Parent);
        //sweeperScene.getStylesheets().add(getClass().getResource("userInterface/Styleprogam2.css").toString());
        ControllerInfo cSC = load2.getController();
        cSC.miMenu.setOnAction(event -> appWindow.setScene(choiceScene));
    }

}
